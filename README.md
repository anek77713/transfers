### 1. Clone.
```
git clone https://anek77713@bitbucket.org/anek77713/transfers.git
```
### 2. Run composer in project dir.
```
composer global require "fxp/composer-asset-plugin:~1.1.1"
composer install
```
### 3. Make dirs writable to the server.
```
chmod -R 777 runtime/
chmod -R 777 web/assets/
```

### 4. Run migrations.
```
./yii migrate
```
### 5. Got to app (have fun)
(register user 'admin' first, this user can see all transfers)

##### Demo url

http://46.101.131.39/transfers/web/

### 6. Tests

Create test database and config db name(transfers_test) in test/codeception

Create in db transfer_test 3 tables using this schemas:
```
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `auth_key` varchar(40) NOT NULL,
  `balance` decimal(10,2) DEFAULT '0.00',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_unique_username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
```
```
CREATE TABLE IF NOT EXISTS `status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
```
```
CREATE TABLE IF NOT EXISTS `transfer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_user_id` int(11) NOT NULL,
  `to_user_id` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `status` int(2) NOT NULL,
  `protection_code` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
```
go to ```/var/www/html/transfers/tests```

run command:
```
php ../vendor/bin/codecept run codeception/unit/models/TransferTest.php
```