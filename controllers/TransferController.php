<?php

namespace app\controllers;

use Yii;
use app\models\Transfer;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\VerbFilter;
use app\models\User;

/**
 * TransferController implements the CRUD actions for Transfer model.
 */
class TransferController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['return', 'protection', 'index', 'view', 'create', 'admin'],
                'rules' => [
                    [
                        'actions' => ['return', 'protection', 'index', 'view', 'create', 'admin'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'return' => ['post'],
                ],
            ],
        ];
    }
    /**
     * Lists all Transfer models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Transfer::find()
                ->where(['to_user_id' => Yii::$app->user->identity->id])
                ->orWhere(['from_user_id' => Yii::$app->user->identity->id]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Transfer models.
     * @return mixed
     */
    public function actionAdmin()
    {
        if (Yii::$app->user->identity->username == 'admin'){
            $dataProvider = new ActiveDataProvider([
                'query' => Transfer::find(),
            ]);

            return $this->render('admin', [
                'dataProvider' => $dataProvider,
            ]);
        }
        else
        {
            throw new ForbiddenHttpException('You are not allowed to access this page.');
        }
    }

    /**
     * Displays a single Transfer model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Transfer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Transfer();

        if ($model->load(Yii::$app->request->post())) {
            $model->from_user_id = Yii::$app->user->identity->id;
            $model->to_user_id = User::getUserIdByUsername(Yii::$app->request->post('Transfer')['user_name']);

            if ($model->protection_code == '')
            {
                $user = User::findIdentity($model->to_user_id);
                $user->balance = $user->balance + $model->amount;

                $intiator = User::findIdentity($model->from_user_id);
                $intiator->balance = $intiator->balance - $model->amount;

                $model->status = 1;

                $user->save();
                $intiator->save();
                $model->save();
            }
            elseif ($model->protection_code != '')
            {
                $model->status = 2;
                $model->save();
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Transfer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionProtection($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->post('Transfer')['protection_code'] != ''
            && $model->status == 2
            && $model->to_user_id == Yii::$app->user->identity->id
        ) {
            if ($model->protection_code == Yii::$app->request->post('Transfer')['protection_code'])
            {
                $user = User::findIdentity($model->to_user_id);
                $user->balance = $user->balance + $model->amount;

                $intiator = User::findIdentity($model->from_user_id);
                $intiator->balance = $intiator->balance - $model->amount;

                $model->status = 1;

                $user->save();
                $intiator->save();
                $model->save();
                \Yii::$app->getSession()->setFlash('success', 'Code ok! Transfer succeeded');
            }
            else
            {
                \Yii::$app->getSession()->setFlash('error', 'Code not ok!');
            }

            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $model->protection_code = '';
            return $this->render('protection', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Transfer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionReturn($id)
    {
        $model = $this->findModel($id);

        if ($model->status == 1 && $model->to_user_id == Yii::$app->user->identity->id){
            $user = User::findIdentity($model->to_user_id);
            $user->balance = $user->balance - $model->amount;

            $intiator = User::findIdentity($model->from_user_id);
            $intiator->balance = $intiator->balance + $model->amount;

            $model->status = 3;

            $user->save();
            $intiator->save();
            $model->save();

            \Yii::$app->getSession()->setFlash('success', 'Transfer returned');
        }

        return $this->redirect(['view', 'id' => $model->id]);
    }

    /**
     * Finds the Transfer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Transfer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Transfer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
