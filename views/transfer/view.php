<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Transfer */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Transfers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transfer-view">

    <h1><?= Html::encode($this->title) ?></h1>

        <?php if(Yii::$app->session->getFlash('error')): ?>
            <div class="alert alert-danger" role="alert">
                <?= Yii::$app->session->getFlash('error'); ?>
            </div>
        <?php endif; ?>

        <?php if(Yii::$app->session->getFlash('success')): ?>
            <div class="alert alert-success" role="success">
                <?= Yii::$app->session->getFlash('success'); ?>
            </div>
        <?php endif; ?>

    <p>


        <?php if($model->status == 2 && $model->to_user_id == Yii::$app->user->identity->id){
            echo Html::a('Enter Protection Code', ['protection', 'id' => $model->id], ['class' => 'btn btn-primary']);
        }
        ?>


        <?php if($model->status == 1 && $model->to_user_id == Yii::$app->user->identity->id){
            echo Html::a('Return', ['return', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to return this transfer?',
                    'method' => 'post',
                ],
            ]);
        }
        ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'userFrom.username',
            'userTo.username',
            'amount',
            'statusName.status_name',
            // 'protection_code',
        ],
    ]) ?>

</div>
