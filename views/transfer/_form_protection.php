<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Transfer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="protection-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'protection_code')->textInput()->label('Protection Code <small>(enter 4-digit code to recive this transfer)</small>') ?>

    <div class="form-group">
        <?= Html::submitButton('Receive', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
