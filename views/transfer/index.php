<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\grid\ActionColumn;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Transfers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transfer-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Transfer', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            [
                'label' => 'From',
                'value' => 'userFrom.username',
            ],
            [
                'label' => 'To',
                'value' => 'userTo.username',
            ],
            'amount',
            [
                'label' => 'Status',
                'value' => 'statusName.status_name',
            ],
            // 'protection_code',

            ['class' => ActionColumn::className(),
            'template' => '{view} {protection} {return}',
            'buttons' => [

                'protection' => function ($url, $model, $key) {
                    if ($model->status == 2 && $model->to_user_id == Yii::$app->user->identity->id)
                    {
                        return Html::a('<span class="glyphicon glyphicon-exclamation-sign"></span>',
                            Url::to(['/transfer/protection', 'id' => $model->id]),
                        [
                            'title' => Yii::t('yii', 'Protection'),
                            'aria-label' => Yii::t('yii', 'Protection'),
                            'data-pjax' => '0',
                        ]
                        );
                    }
                },

                'return' => function ($url, $model, $key) {
                    if ($model->status == 1 && $model->to_user_id == Yii::$app->user->identity->id)
                    {
                        return Html::a('<span class="glyphicon glyphicon-share-alt"></span>',
                            Url::to(['/transfer/return', 'id' => $model->id]),
                        [
                            'title' => Yii::t('yii', 'Return'),
                            'data-confirm' => 'Are you sure you want to return this transfer?',
                            'data-method' => 'post',
                            'data-pjax' => '0',
                        ]);
                    }
                },
            ],
            ],

        ],
    ]); ?>
</div>