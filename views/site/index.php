<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'All users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'id',
        'username',
        'balance',
    ],
]); ?>
</div>
