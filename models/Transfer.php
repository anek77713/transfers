<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "transfer".
 *
 * @property integer $id
 * @property integer $from_user_id
 * @property integer $to_user_id
 * @property string $amount
 * @property integer $status
 * @property integer $protection_code
 */
class Transfer extends \yii\db\ActiveRecord
{

    public $user_name;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transfer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from_user_id', 'to_user_id', 'amount', 'status'], 'required'],
            [['from_user_id', 'to_user_id', 'status', 'protection_code'], 'integer'],
            [['amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'from_user_id' => 'From User ID',
            'to_user_id' => 'To User ID',
            'amount' => 'Amount',
            'status' => 'Status',
            'protection_code' => 'Protection Code',
        ];
    }

    public function getStatusName(){
      return $this->hasOne(Status::className(), ['id' => 'status']);
    }

    public function getUserFrom(){
      return $this->hasOne(User::className(), ['id' => 'from_user_id']);
    }

    public function getUserTo(){
      return $this->hasOne(User::className(), ['id' => 'to_user_id']);
    }

}