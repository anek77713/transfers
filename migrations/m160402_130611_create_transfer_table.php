<?php

use yii\db\Schema;
use yii\db\Migration;

class m160402_130611_create_transfer_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%transfer}}', [
            'id' => Schema::TYPE_PK,
            'from_user_id'             => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'to_user_id'             => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'amount'           => Schema::TYPE_DECIMAL . '(10,2) NOT NULL',
            'status'           => Schema::TYPE_INTEGER . '(2) NOT NULL',
            'protection_code'           => Schema::TYPE_INTEGER . '(4) DEFAULT NULL',
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%transfer}}');
    }
}
