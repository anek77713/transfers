<?php

use yii\db\Schema;
use yii\db\Migration;

class m160402_124523_create_status_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%status}}', [
            'id' => Schema::TYPE_PK,
            'status_name'             => Schema::TYPE_STRING . '(15) NOT NULL',
        ], $tableOptions);

        $this->batchInsert('status', ['status_name'], [
                ["transfer"],
                ["pending"],
                ["return"],
            ]);
    }

    public function down()
    {
        $this->dropTable('{{%status}}');
    }
}
