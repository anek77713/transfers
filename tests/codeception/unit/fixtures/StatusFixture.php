<?php

namespace app\tests\codeception\unit\fixtures;
use yii\test\ActiveFixture;

class StatusFixture extends ActiveFixture
{
    public $modelClass = 'app\models\Status';
}
