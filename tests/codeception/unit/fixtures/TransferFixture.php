<?php

namespace app\tests\codeception\unit\fixtures;
use yii\test\ActiveFixture;

class TransferFixture extends ActiveFixture
{
    public $modelClass = 'app\models\Transfer';
}
