<?php

return [
    [
        'id' => 1,
        'from_user_id' => 1,
        'to_user_id' => 2,
        'amount' => '10',
        'status' => 1,
        'protection_code' => 1111,
    ],
    [
        'id' => 2,
        'from_user_id' => 2,
        'to_user_id' => 1,
        'amount' => '22.2',
        'status' => 2,
        'protection_code' => 1111,
    ],
    [
        'id' => 3,
        'from_user_id' => 1,
        'to_user_id' => 2,
        'amount' => '10',
        'status' => 3,
        'protection_code' => 1111,
    ],
]
?>