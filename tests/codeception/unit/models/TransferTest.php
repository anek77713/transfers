<?php

namespace app\tests\codeception\unit\models;

use yii\codeception\TestCase;
use app\models\Transfer;
use app\tests\codeception\unit\fixtures\TransferFixture;
use app\tests\codeception\unit\fixtures\UserFixture;
use app\tests\codeception\unit\fixtures\StatusFixture;
use Codeception\Util\Debug;

class TransferTest extends TestCase
{

    public function testStatusName()
    {

        $transfer = Transfer::findOne(['id' => 1]);
        $this->assertTrue($transfer->statusName->status_name == 'transfer');

        $transfer = Transfer::findOne(['id' => 2]);
        $this->assertTrue($transfer->statusName->status_name == 'pending');

        $transfer = Transfer::findOne(['id' => 3]);
        $this->assertTrue($transfer->statusName->status_name == 'return');
    }

    public function testUserFrom()
    {
        $transfer = Transfer::findOne(['id' => 1]);
        $this->assertTrue($transfer->userFrom->username == 'admin');
    }

    public function testUserTo()
    {
        $transfer = Transfer::findOne(['id' => 1]);
        $this->assertTrue($transfer->userTo->username == 'user');
    }

    public function fixtures(){
        return [
            'user' => UserFixture::className(),
            'transfer' => TransferFixture::className(),
            'status' => StatusFixture::className(),
        ];
    }
}